module.exports = function (app) {
  // Import du modèle
  var Errors = require('../models/errors')

  /**
   * Route /
   */
  app.route('/')
  .get(Errors.welcome)
}