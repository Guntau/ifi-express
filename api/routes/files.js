module.exports = function (app) {
  // Import du modèle
  var Files = require('../models/files')

  /**
   * Route /file
   */
  app.route('/file')
  .get(Files.get_file)
}