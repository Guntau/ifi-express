module.exports = function (app) {
  // Import du modèle
  var Text = require('../models/text')

  /**
   * Route /text
   */
  app.route('/text')
  .get(Text.get_text)
}