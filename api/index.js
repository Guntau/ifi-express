module.exports = function(app) {

  const fs = require('fs');

  /**
   * Instancie dynamiquement les nouvelles routes à partir des noms de fichiers
   */
  fs.readdir('./api/routes/', (err, files) => {
    if(err) throw err;

    files.forEach(file => {
      var route = require('./routes/' + file)
      route(app);
    });
  });
}