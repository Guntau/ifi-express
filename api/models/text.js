module.exports = {

  /**
   * GET /text
   * Retourne le texte du fichier data.text/txt
   */
  get_text (req, res) {
    const fs = require('fs');

    fs.readFile('./data/text.txt', 'utf8', (err, data) => {
      if (err) throw err;
      res.status('200').json({"text":data})
    });

  }

}