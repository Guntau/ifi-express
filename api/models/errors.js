module.exports = {

  /**
   * GET /
   * Retourne un message d'accueil
   */
  welcome (req, res) {
    res.status('200').send("Bienvenue sur ton API rest !")
  }

}