var express = require("express");
var bodyParser = require("body-parser");
var routes = require("./api");
var app = express();

// On parse les json et les requêtes encodées
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Headers par défaut
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    next();
});

// Instanciation des routes
routes(app);

// Démarrage du serveur
var server = app.listen(3000, function () {
    console.log("app running on port.", server.address().port);
});