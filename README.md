#ifi-express

Corentin Bernard
Alexis Dupuis

## Installation | Getting started
Une fois le projet cloné placez vous à l'intérieur lancez la commande ``` npm install ``` pour installer les dépendances.
Une fois installé utilisez la commande ``` npm run dev ``` pour lancer le serveur.
Vous pouvez tester vos routes sur ```http://localhost:3000```
*Le serveur se relance automatiquement si vous modifiez les fichiers, cela vous évite de devoir le relancer à chaque fois* :)

## Architecture

- ``` index.js ``` Est la racine du serveur, il instancie les routes et lance le serveur

- ``` api ``` Contient les routes et les modèles
  -  ``` models/ ``` Contient la logique derrière chaque requête
  - ``` routes/ ``` Contient les routes pour lesquels les requêtes sont appliquées

- ``` data/ ``` Stocke les fichiers



## Questions 

#### 1)
Démarrer le serveur (on va commencer gentiment hein).

#### 2)
Créer une route ```GET /bonjour ``` qui retourne le message: ```Bonjour !```

#### 3)
Créer une route ```GET /bonjour/{nom} ``` qui retourne le message: ```Bonjour {nom} !```

#### 4)
Créer une route ```POST /text ``` qui remplace le texte du fichier ```data/text.txt ``` par le texte contenu dans le champ ```text``` du body de la requête.

#### 5)
Créer une route ```PUT /text ``` qui ajoute au texte du fichier ```data/text.txt ``` le texte contenu dans le champ ```text``` du  body.

#### 6)
Modifier  ```models/files.js ``` et ```routes/files.js ``` pour que la route ```GET /file ``` retourne le fichier ```data/text.txt ``` à télécharger.

#### 7)
Ajouter une route ```POST /file ``` qui permet d'upload un fichier, qui sera enregistré dans le dossier ``` data/ ```

( vous aurez probablement besoin de [ceci](https://www.npmjs.com/package/formidable))

#### 8)
Ajouter une route ``` DELETE /file/{name} ``` qui permet de supprimer le fichier ```{name}``` du serveur

#### 9)
Ajouter une route ``` GET /file/{name} ``` qui permet de retourner le fichier ```{name}``` du serveur.
Il faudra bien évidemment gérer le cas ou le fichier a été supprimé.

Faites aussi attention à vérifier quel fichier est supprimé..

## Partie 2

Réaliser un chat accessible sur ``` /chat ```
Pour cela vous pourrez utiliser [ejs](http://ejs.co/) pour produire un template, et [socket.io](https://socket.io/) pour gérer les connexions

## Partie 3

Ecrire une route ```POST /compile ``` qui retourne une version compilé du fichier passé en paramètre.

## Notes
console.log() est votre meilleur ami !

Vous pouvez aussi utilisez [Postman](https://www.getpostman.com/) pour tester vous même vos requêtes !